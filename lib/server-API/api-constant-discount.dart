//API DISCOUNT URL FROM BLOCK-CHAIN-SERVER

const String localhost = 'http://localhost:8080/api/auth';

//Role CUSTOMER
const String getAllDiscountCodeOfUserByUserId = '$localhost/get-all-discount-code-of-user-by-userId';
const String getDiscountCodeOfUserById = '$localhost/get-discount-code-of-user-by-id/{discountCodeOfUserId}';
const String createDiscountCodeOfUser = '$localhost/create-discount-code-of-user';
const String deleteDiscountCode = '$localhost/delete-discount-code-of-user/{discountCodeOfUserId}';
const String getDiscountCodeByCode = '$localhost/get-discount-code-by-code';

const String newapi = '$localhost/...'; 
