//API AUTHENTICATION URL FROM BLOCK-CHAIN-SERVER
const String localhost = 'http://localhost:8080/api/auth';
const String authV2 = '$localhost/sign-in-v2';
const String authV1 =  '$localhost/sign-in-v1';
const String registration =  '$localhost/sign-up';
const String verifyAccount = '$localhost/verify';
const String signOut = '$localhost/sign-out';
const String refreshToken = '$localhost/refresh-token';
const String forgotPassword = '$localhost/forgot-password';

const String newapi = '$localhost/...';

