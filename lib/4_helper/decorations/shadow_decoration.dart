import 'package:blockchain_mobile/constants.dart';
import 'package:flutter/cupertino.dart';

const shadowDecoration = BoxDecoration(boxShadow: [
  BoxShadow(
    color: kHintTextColor,
    spreadRadius: 2,
    blurRadius: 2,
  )
]);
