import 'package:blockchain_mobile/1_controllers/repositories/auth_repository.dart';
import 'package:mockito/mockito.dart';

class AuthRepositoryMock extends Mock implements AuthRepository {}
